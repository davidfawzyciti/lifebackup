﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace LifeBackup.Api.Controllers
{
    [EnableCors("AllowOrigin")]
    public class HomeController : Controller
    {
        
        private readonly IConfiguration _configuration;
        private string baseUrl = string.Empty;
        public HomeController(IConfiguration configuration)
        {
            _configuration = configuration;
            baseUrl= _configuration["apiSettings:apiBaseUrl"];
        }
        public IActionResult Index()
        {
            ViewBag.ApiBaseUrl = baseUrl;
            return View();
        }

        public IActionResult BucketManager()
        {
            ViewBag.ApiBaseUrl = baseUrl;
            return View();
        }

        public IActionResult ObjectManager()
        {
            ViewBag.ApiBaseUrl = baseUrl;
            return View();
        }

        public IActionResult FileManager()
        {
            ViewBag.ApiBaseUrl = baseUrl;
            return View();
        }
        public IActionResult Privacy()
        {
            ViewBag.ApiBaseUrl = baseUrl;
            return View();
        }
    }
}
