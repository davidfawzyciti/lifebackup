﻿using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using LifeBackup.Core.Communication.Files;
using LifeBackup.Core.Interfaces;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using ILogger = Amazon.Runtime.Internal.Util.ILogger;

namespace LifeBackup.Infrastructure.Repositories
{
    public class FilesRepository : IFilesRepository
    {

        private readonly IAmazonS3 _s3Client;
        private readonly ILogger<FilesRepository> _logger;
        public FilesRepository(IAmazonS3 s3Client , ILogger<FilesRepository> logger)
        {
            _s3Client = s3Client;
            _logger = logger;
        }

        public async Task<AddFileResponse> UploadFiles(string bucketName, IList<IFormFile> formFiles)
        {
            var response = new List<string>();

            foreach (var file in formFiles)
            {
                var uploadRequest = new TransferUtilityUploadRequest
                {
                    InputStream = file.OpenReadStream(),
                    Key = file.FileName,
                    BucketName = bucketName,
                    CannedACL = S3CannedACL.NoACL
                };

                using (var fileTransferUtility = new TransferUtility(_s3Client))
                {
                    await fileTransferUtility.UploadAsync(uploadRequest);
                }

                var expiryUrlRequest = new GetPreSignedUrlRequest
                {
                    BucketName = bucketName,
                    Key = file.FileName,
                    Expires = DateTime.Now.AddDays(1)
                };

                var url = _s3Client.GetPreSignedURL(expiryUrlRequest);

                response.Add(url);
            }

            return new AddFileResponse
            {
                PreSignedUrl = response
            };
        }

        public async Task<IEnumerable<ListFilesResponse>> ListFiles(string bucketName)
        {
            var responses = await _s3Client.ListObjectsAsync(bucketName);

            return responses.S3Objects.Select(b => new ListFilesResponse
            {
                BucketName = b.BucketName,
                Key = b.Key,
                Owner = b.Owner.DisplayName,
                Size = b.Size
            });
        }

        public async Task DownloadFile(string bucketName, string fileName)
        {
            var pathAndFileName = $"C:\\S3Temp\\{fileName}";

            var downloadRequest = new TransferUtilityDownloadRequest
            {
                BucketName = bucketName,
                Key = fileName,
                FilePath = pathAndFileName
            };

            using (var transferUtility = new TransferUtility(_s3Client))
            {
                await transferUtility.DownloadAsync(downloadRequest);
            }
        }

        public async Task<DeleteFileResponse> DeleteFile(string bucketName, string fileName)
        {
            var multiObjectDeleteRequest = new DeleteObjectsRequest
            {
                BucketName = bucketName
            };

            multiObjectDeleteRequest.AddKey(fileName);

            var response = await _s3Client.DeleteObjectsAsync(multiObjectDeleteRequest);

            return new DeleteFileResponse
            {
                NumberOfDeletedObjects = response.DeletedObjects.Count
            };
        }

        public async Task<PutObjectRequest> AddJsonObject(string bucketName, AddJsonObjectRequest request)
        {
            try
            {
                var createdOnUtc = DateTime.UtcNow;

                var s3Key = $"{createdOnUtc:yyyy}/{createdOnUtc:MM}/{createdOnUtc:dd}/{request.Id}";

                var putObjectRequest = new PutObjectRequest
                {
                    BucketName = bucketName,
                    Key = s3Key,
                    ContentBody = JsonConvert.SerializeObject(request)
                };

                await _s3Client.PutObjectAsync(putObjectRequest);
                return putObjectRequest;
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                _logger.LogError(e,"Adding object Error");
                throw;
            }
            
        }

        public async Task<GetJsonObjectResponse> GetJsonObject(string bucketName, string fileName)
        {
            var request = new GetObjectRequest
            {
                BucketName = bucketName,
                Key = fileName
            };

            var response = await _s3Client.GetObjectAsync(request);

            using (var reader = new StreamReader(response.ResponseStream))
            {
                var contents = reader.ReadToEnd();
                return JsonConvert.DeserializeObject<GetJsonObjectResponse>(contents);
            }
        }

        public async Task<DeleteObjectResponse> DeleteJsonObject(string bucketName, string fileKey)
        {
            try
            {
                var deleteObjectRequest = new DeleteObjectRequest
                {
                    BucketName = bucketName,
                    Key = fileKey
                };

                _logger.LogInformation("Deleting an object");
                var response = await _s3Client.DeleteObjectAsync(deleteObjectRequest);
                return response;
            }
            catch (AmazonS3Exception e)
            {
                _logger.LogCritical("Error encountered on server. Message:'{0}' when deleting an object", e.Message);
                throw e;
            }
            catch (Exception e)
            {
                _logger.LogCritical("Unknown encountered on server. Message:'{0}' when deleting an object", e.Message);
                throw e;
            }
            
        }
    }
}
