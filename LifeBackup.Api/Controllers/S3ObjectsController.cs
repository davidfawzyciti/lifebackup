﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using LifeBackup.Core.Communication.Files;
using LifeBackup.Core.Interfaces;
using Microsoft.AspNetCore.Cors;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace LifeBackup.Api.Controllers
{
    [EnableCors("AllowOrigin")]
    [Route("api/S3Objects")]
    [ApiController]
    public class S3ObjectsController : ControllerBase
    {
        private readonly IFilesRepository _filesRepository;

        public S3ObjectsController(IFilesRepository filesRepository)
        {
            _filesRepository = filesRepository;
        }
        [HttpPost]
        [Route("{bucketName}/addjsonobject")]
        public async Task<IActionResult> AddJsonObject([FromRoute] string bucketName, [FromForm] AddJsonObjectRequest request)
        {
            var result =await _filesRepository.AddJsonObject(bucketName, request);

            return Ok(result);
        }

        [HttpGet]
        [Route("{bucketName}/getjsonobject")]
        public async Task<ActionResult<GetJsonObjectResponse>> GetJsonObject([FromRoute] string bucketName, string fileName)
        {
            var response = await _filesRepository.GetJsonObject(bucketName, fileName);

            return Ok(response);
        }

        [HttpPost]
        [Route("{bucketName}/deleteObject")]
        public async Task<IActionResult> DeleteS3Object([FromRoute] string bucketName, [FromForm] string delFileName)
        {
            var response = await _filesRepository.DeleteJsonObject(bucketName, delFileName);

            return Ok(response);
        }
    }
}
